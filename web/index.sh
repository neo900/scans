#!/bin/sh

. ./style.sh


cat <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<TITLE>N900 Case Scans</TITLE>
<BODY>

<H1>N900 Case Scans</H1>

These are scans of mechanical parts of the N900 phone.
They are intended for designing mechanically compatible elements, such
as the Neo900 spacer frame.
<P>
The available groups of scans and pictures:
<UL>
  <LI> The inside of the bottom shell:
    <A href="#bottom-inside">with stylus</A>,
    <A href="#bottom-inside-nostylus">without stylus</A>, and
    <A href="#bottom-assembled-inside">assembled</A>
    (i.e., with battery, cover, and stylus).
  <LI> Elements of the keyboard:
    <A href="#key-top">key mat</A> (top only),
    <A href="#key-frame-inside">keyboard frame</A> (inside only),
    <A href="#keytop-inside">both together</A> (only pictures).
  <LI> The top of the <A href="#base-top">fully assembled base</A>,
    <A href="#base-nokbd">without keyboard</A>.
  <LI> The main PCB:
    <A href="#pcb-top">top</A> with daughterboard folded away,
    <A href="#pcb-bottom">bottom</A> with daughterboard on top or folded away,
    and the daughterboard alone,
    <A href="#pcb-daughter-top">top</A> and
    <A href="#pcb-daughter-bottom">bottom</A>.
  <LI> The display module: the inside, with the sliding mechanism in the
    <A href="#disp-closed-inside">closed</A> and in the
    <A href="#disp-open-inside">open</A> position.
  <LI> The rear side of the device, with the battery cover open and
    the battery
    <A href="#rear-open-nobat">absent</A> or
    <A href="#rear-open-bat">present</A>.
</UL>
<P>
Scans are stored in the following file formats:
PIX, the text format of Dr.PICZA project files, and
<A href="http://en.wikipedia.org/wiki/STL_(file_format)">STL</A>.
Clicking on the thumbnail image will show a larger version of the
3D rendering, generated with
<A href="http://meshlab.sourceforge.net/">MeshLab</A>.<P>
High resolution scans can produce a large amount of data.
If a file is 100 kB or bigger, its size is indicated.
EOF

# -----------------------------------------------------------------------------

section bottom-inside "Bottom shell, with stylus"

cat <<EOF
The bottom shell is the main structure of the N900 case. The following two
pictures show it with the stylus partially inserted, the way it ended up
after the 100 &mu;m scan. Note that the scans do not quite reach the bottom
of the shell and some deep areas that are closed therefore appear to be
open.
<BR>
Click on the pictures to enlarge.
<P>
EOF
photo n900-bottom-inside-top
photo n900-bottom-inside-angle

scan_set n900-bot-inside "Bottom shell. Inside."
explain_set <<EOF
Note: in the 100 &mu;m scan the scanning head partially pushed the stylus
out of the case.<BR>
The stylus therefore appears to be distorted.<BR>
Also note that the scans did not go all the way to the bottom of the
case shell.<BR>
Some of the holes therefore simply represent a floor that has been
out of reach.<BR>
The "Bottom shell, assembled" scans below are more accurate in this
regard.
EOF
images3
texts3 "1.5 h" "3.25 h" "50 h"

# -----------------------------------------------------------------------------

section bottom-inside-nostylus "Bottom shell, without stylus"

cat <<EOF
With the stylus completely removed, the case looks like this:
<P>
EOF
photo n900-bottom-inside-nostylus-top
photo n900-bottom-inside-nostylus-angle

scan_set n900-bot-inside-nostylus "Bottom shell without stylus. Inside."
image3rd
text3rd "12 h"

# -----------------------------------------------------------------------------

section bottom-assembled-inside "Bottom shell, assembled"

cat <<EOF
This is another set of scans of the bottom half, but this time with the
usual bottom-side features (battery, two small sub-parts of the bottom
shell, battery cover) present.
<P>
EOF
photo n900-bot-assembled-inside-top

scan_set n900-bot-assembled-inside "Bottom shell, assembled. Inside."
images3
texts3 "1.5 h" "3.5 h" "53 h"

# -----------------------------------------------------------------------------

section key-top "Key mat"

cat <<EOF
The key mat is an assembly consisting of the key caps mounted on a rubber
sheet, stabilized with a sheet of steel. Note that a small piece of dirt
was stuck between the X and C key, and was picked up in the scans.
<P>
EOF
photo n900-key-top
photo n900-key-top-support

scan_set n900-key-top "Key mat. Top."
images4
texts4 "0.5 h" "1.2 h" "12 h" "42.5 h"

# -----------------------------------------------------------------------------

section key-frame-inside "Keyboard frame"

cat <<EOF
The keyboard frame is a fragile plastic structure that is attached at the
top of the bottom shell and holds the keyboard in place.<P>
In Neo900, the keyboard frame will be integrated into the spacer frame.
<P>
EOF
photo n900-frame-inside-top
photo n900-frame-inside-angle

scan_set n900-frame-inside "Keyboard frame. Inside."
explain_set <<EOF
Note: the keyboard frame features little "horns" that snap into the
bottom case shell.<BR>
These horns and the narrow (about 1.6 mm wide) plastic
strip they attach to easily bend away from the scanner's needle
and are thus only partially visible in the scans.
EOF
images3
texts3 "0.5 h" "1.3 h" "10.5 h"

# -----------------------------------------------------------------------------

section keytop-inside "Key mat in keyboard frame"

cat <<EOF
The following pictures illustrate how the key mat fits in the keyboard frame.
<P>
EOF
photo n900-keytop-inside
photo n900-keytop-outside

# -----------------------------------------------------------------------------

section base-top "Assembled base"

cat <<EOF
The fully assembled bottom half of the N900, consisting of bottom shell, PCB,
battery, battery cover, key mat, keyboard frame, and two smaller plastic items.
<P>
EOF
photo n900-base-angle-left
photo n900-base-top
photo n900-base-angle-right

scan_set n900-base-top "Assembled base. Top."
images3
texts3 "1.1 h" "4 h" "42.3 h"

scan_set n900-base-top "Assembled base with enhanced details. Top."
explain_set <<EOF
Overall X/Y step is 100 &mu;m but in the area where the keyboard frame
ends and the PCB begins a ~10 mm high band was re-scanned at 50 &mu;m.
EOF
image "$__id-50-100um"
text 50-100um <<EOF
X/Y step size: 50/100 &mu;m.<BR>
Z resolution: 25 &mu;m<BR>
Approximate extra scan time: 24 h<BR>
EOF

# -----------------------------------------------------------------------------

section base-nokbd "Assembled base without keyboard"

cat <<EOF
The fully assembled bottom half of the N900, consisting of bottom shell, PCB,
battery, battery cover, and two smaller plastic items. Without key mat and
keyboard frame.
<P>
EOF
photo n900-base-nokbd-top
photo n900-base-nokbd-inside
photo n900-base-nokbd-inside-center
photo n900-base-nokbd-inside-corner

scan_set n900-base-nokbd "Assembled base without keyboard. Top."
images3
texts3 "1.1 h" "2.3 h" "30 h"

# -----------------------------------------------------------------------------

section pcb-top "The PCB, top"

photo n900-pcb-top
cat <<EOF
<BR>
<A href="${BASE}pics/large/n900-pcb-top-hires.jpg">Full resolution
(3552 x 1536)</A>
EOF

scan_set n900-pcb-top "PCB with daughterboard. Top."
images3
texts3 "1.6 h" "4 h" "47.5 h"

# -----------------------------------------------------------------------------

section pcb-bottom "The PCB, bottom"

photo n900-pcb-stacked-bottom
cat <<EOF
<BR>
<A href="${BASE}pics/large/n900-pcb-stacked-bottom-hires.jpg">Full resolution
(3636 x 1920)</A>
EOF

scan_set n900-pcb-stacked-bottom "PCB with daughterboard. Bottom."
images3
texts3 "1.3 h" "4 h" "43 h"

flush

photo n900-pcb-unstacked-bottom

scan_set n900-pcb-unstacked-bottom "PCB with daughterboard folded away. Bottom."
image4th
text4th "138 h"

# -----------------------------------------------------------------------------

section pcb-daughter-top "The daughterboard, top"

photo n900-daughter-top
cat <<EOF
<BR>
<A href="${BASE}pics/large/n900-daughter-top-hires.jpg">Full resolution
(2624 x 2048)</A>
EOF

scan_set n900-daughter-top "Daughterboard alone. Top."
explain_set <<EOF
Note: the flash LEDs are covered with a soft resin that confused the
scanner and made them look much wider and with ragged edges in the
X+ direction. (Y+ in the pictures.)
EOF
images1
image4th
text1st "0.1 h"
text4th "17.9 h"

# -----------------------------------------------------------------------------

section pcb-daughter-bottom "The daughterboard, bottom"

photo n900-daughter-bottom

scan_set n900-daughter-bottom "Daughterboard alone. Bottom."
explain_set <<EOF
The bottom of the daughterboard is covered with a soft foam that confused
the scanner and caused the ragged edges in the X+ direction. Note that
the part scanned shows some damage from repair attempts of the previous
owner. The diagonal item in the camera opening is a support structure
added for scanning and not part of the daughterboard.
EOF
images1
image3rd
text1st "0.2 h"
text3rd "6 h"

# -----------------------------------------------------------------------------

section disp-closed-inside "The display module, slider in the closed position"

photo n900-disp-bottom-top
photo n900-disp-bottom-angle

scan_set n900-disp-inside "The display module. Closed."
images3
texts3 "1.2 h" "3.8 h" "44 h"

# -----------------------------------------------------------------------------

section disp-open-inside "The display module, slider in the open position"

photo n900-disp-open-bottom-top

scan_set n900-disp-open-inside "The display module. Open."
images3
texts3 "1.2 h" "9.8 h" "45 h"

# -----------------------------------------------------------------------------

section rear-open-nobat "Rear side, without battery "

cat <<EOF
Please note that the screws in this device are not the Nokia originals
and differ in size and type. <B>Do not</B> use these pictures or scans
to determine screw placement in your device !
<P>
EOF

photo n900-rear-open-nobat-top

scan_set n900-rear-open-nobat "The rear side. Open, no battery."
images3
texts3 "1.2 h" "4.9 h" "53 h"

# -----------------------------------------------------------------------------

section rear-open-bat "Rear side, with battery "

cat <<EOF
Please note that the screws in this device are not the Nokia originals
and differ in size and type. <B>Do not</B> use these pictures or scans
to determine screw placement in your device !
<P>
EOF

photo n900-rear-open-bat-angle

scan_set n900-rear-open-bat "The rear side. Open, with battery."
image2nd
text2nd "4.2 h"

# -----------------------------------------------------------------------------

end

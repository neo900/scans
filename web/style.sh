#!/bin/sh

if [ -z "$BASE" ]; then
#	BASE="https://neo900.org/git/?p=scans;a=blob_plain;hb=HEAD;f="
	BASE="https://neo900.org/git/scans/plain/"
fi

BG=#e4e4e0
TITLE=#ffd0b0
EXPLAIN=#ffff90


# ----- section ---------------------------------------------------------------


flush()
{
    echo "$__before_set"
    __before_set=
    echo "<P>"
}


section()
{
    flush
    cat <<EOF
<HR>
<P>
<A name="$1">
<H2>$2</H2>
EOF
}


# ----- scan_set / images / texts ---------------------------------------------


scan_set()
{
    echo "$__before_set"
    __id=$1
    __before_set="</TABLE></TABLE>"
    __before_image="<TR><TD><TABLE bgcolor="$BG" border=0 cellspacing=3 cellpadding=2><TR>"
    __before_text="<TR>"
    cat <<EOF
<P>
<A name="$1">
<TABLE border=0 cellspacing=0 cellpadding=0>
  <TR>
    <TD>
      <TABLE border=1 cellspacing=0 width="100%">
	<TR>
      </TABLE>
  <TR>
    <TD>
      <TABLE bgcolor="$TITLE" border=0 cellspacing=3 cellpadding=2
        width="100%">
        <TR>
          <TD><B>$2</B>
      </TABLE>
EOF
}


__do_explain()
{
    cat <<EOF
    <TABLE bgcolor="$BG" border=0 cellspacing=3 cellpadding=2 width="100%">
      <TR>
        <TD>
	  <TABLE bgcolor="$EXPLAIN" border=0 cellspacing=3 cellpadding=2>
	    <TR>
	      <TD><I>
EOF
    cat
    cat <<EOF
		</I>
	  </TABLE>
    </TABLE>
EOF
}


explain_set()
{
    cat <<EOF
<TR>
  <TD>
EOF
    __do_explain
}


__do_before_image()
{
    echo "$__before_image"
    __before_image=
}


image()
{
    __do_before_image
    cat <<EOF
<TD><A href="${BASE}data/png/$1.png"><IMG src="${BASE}data/jpg/$1.jpg"></A>
EOF
}


images1()
{
    image "$__id-1mm"
}


image2nd()
{
    image "$__id-500um"
}


image3rd()
{
    image "$__id-100um"
}


image4th()
{
    image "$__id-50um"
}


images2()
{
    images1
    image2nd
}


images3()
{
    images2
    image3rd
}


images4()
{
    images3
    image4th
}


under_consideration()
{
    __do_before_image
    cat <<EOF
<TD><H1>Under consideration</H1>
EOF
}


planned()
{
    __do_before_image
    cat <<EOF
<TD><H1>Planned</H1>
EOF
}


in_progress()
{
    __do_before_image
    cat <<EOF
<TD><H1>In progress</H1>
EOF
}


link()
{
    [ -r "../$1" ] || return
    size=`ls -l ../$1 | awk '{printf $5}'`
    if [ $size -lt 95000 ]; then
	size="`echo $size | awk '{printf("%.0f kB", $1/1000)}'`"
	size=
    else
	size="`echo $size | awk '{printf("%.1f MB", $1/1000000)}'`"
    fi
    echo "<A href=\"${BASE}$1\">$2</A> $size"
}


__text()
{
    echo "$__before_text"
    __before_text=
    echo "<TD>"
    cat
    link "data/pix/$1.pix.bz2" PIX
    link "data/stl/$1.stl.bz2" STL
}


text()
{
    __text "$__id-$1"
}


text1st()
{
    text 1mm <<EOF
X/Y step size: 1 mm.<BR>
Z resolution: <B>DRAFT</B><BR>
Approximate scan time: $1<BR>
EOF
}


text2nd()
{
    text 500um <<EOF
X/Y step size: 500 &mu;m.<BR>
Z resolution: 25 &mu;m.<BR>
Approximate scan time: $1<BR>
EOF
}


texts1()
{
    text1st "$1"
}


texts2()
{
    text1st "$1"
    text2nd "$2"
}


text3rd()
{
    text 100um <<EOF
X/Y step size: 100 &mu;m.<BR>
Z resolution: 25 &mu;m.<BR>
Approximate scan time: $1<BR>
EOF
}


texts3()
{
    texts2 "$1" "$2"
    text3rd "$3"
}


text4th()
{
    text 50um <<EOF
X/Y step size: 50 &mu;m.<BR>
Z resolution: 25 &mu;m.<BR>
Approximate scan time: $1<BR>
EOF
}


texts4()
{
    texts3 "$1" "$2" "$3"
    text4th "$4"
}


# ----- scan_group / scan -----------------------------------------------------


scan_group()
{
    echo "$__before_set"
    __before_set="</TABLE>"
    __before_hdr="<TR>"
    __before_img="<TR>"
    __before_text="<TR>"
    cat <<EOF
<P>
<TABLE bgcolor="$BG" border=0 cellspacing=0 cellpadding=0>
  <TR>
EOF
}


scan_bar()
{
cat <<EOF
<TD>
  <TABLE border=1 cellspacing=0 width="100%">
    <TR>
  </TABLE>
EOF
}


scan_hdr()
{
    echo "$__before_hdr"
    __before_hdr=
cat <<EOF
<TD>
  <TABLE bgcolor="$TITLE" border=0 cellspacing=3 cellpadding=0 width="100%">
    <TR>
      <TD><B>$1</B>
    </TABLE>
EOF
}


scan_img()
{
    echo "$__before_img"
    __before_img=
cat <<EOF
<TD>
  <TABLE border=0 cellspacing=3 cellpadding=0>
    <TR>
      <TD><IMG src="${BASE}data/jpg/$1.jpg">
  </TABLE>
EOF
}


scan_text()
{
    echo "$__before_text"
    __before_text=
    echo "<TD>"
    if [ ! -z "$3" ]; then
	echo "$3" | __do_explain
    fi
    cat <<EOF
<TABLE border=0 cellspacing=3 cellpadding=2>
  <TR>
EOF
    __text $1 <<EOF
Approximate scan time: $2<BR>
X/Y step size: 100 &mu;m.<BR>
Z resolution: 25 &mu;m.<BR>
EOF
    echo "</TABLE>"
}


# ----- photos ----------------------------------------------------------------


photo()
{
    cat <<EOF
<A href="${BASE}pics/large/$1.jpg"><IMG src="${BASE}pics/small/$1.jpg"></A>
EOF
}


# ----- end -------------------------------------------------------------------


end()
{
    echo "$__before_set"
    cat <<EOF
<P>
<HR>
<P>
`date -u '+%F %X'` UTC
</BODY>
</HTML>
EOF
}
